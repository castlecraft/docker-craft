import { Test, TestingModule } from '@nestjs/testing';
import { TcpCheckService } from '../../services/tcp-check/tcp-check.service';
import { HealthzController } from './healthz.controller';

describe('HealthzController', () => {
  let controller: HealthzController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HealthzController],
      providers: [{ provide: TcpCheckService, useValue: {} }],
    }).compile();

    controller = module.get<HealthzController>(HealthzController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
