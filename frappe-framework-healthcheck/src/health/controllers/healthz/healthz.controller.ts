import { Controller, Get } from '@nestjs/common';
import { HealthCheck } from '@nestjs/terminus';
import { TcpCheckService } from '../../services/tcp-check/tcp-check.service';

@Controller('healthz')
export class HealthzController {
  constructor(private readonly tcpCheck: TcpCheckService) {}

  @Get()
  @HealthCheck()
  check() {
    return this.tcpCheck.check();
  }
}
