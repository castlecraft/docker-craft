import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Transport } from '@nestjs/microservices';
import {
  HealthCheckService,
  MicroserviceHealthIndicator,
} from '@nestjs/terminus';
import {
  SOCKETIO_HOST,
  SOCKETIO_PORT,
  MARIADB_HOST,
  MARIADB_PORT,
  REDIS_SOCKETIO_HOST,
  REDIS_SOCKETIO_PORT,
  REDIS_CACHE_HOST,
  REDIS_CACHE_PORT,
  REDIS_QUEUE_HOST,
  REDIS_QUEUE_PORT,
} from '../../../config-options';

@Injectable()
export class TcpCheckService {
  constructor(
    private health: HealthCheckService,
    private tcp: MicroserviceHealthIndicator,
    private config: ConfigService,
  ) {}

  check() {
    return this.health.check([
      () =>
        this.tcp.pingCheck('erpnext-socketio', {
          transport: Transport.TCP,
          options: {
            host: this.config.get<string>(SOCKETIO_HOST),
            port: Number(this.config.get<string>(SOCKETIO_PORT)),
          },
        }),
      () =>
        this.tcp.pingCheck('mariadb', {
          transport: Transport.TCP,
          options: {
            host: this.config.get<string>(MARIADB_HOST),
            port: Number(this.config.get<string>(MARIADB_PORT)),
          },
        }),
      () =>
        this.tcp.pingCheck('redis-socketio', {
          transport: Transport.TCP,
          options: {
            host: this.config.get<string>(REDIS_SOCKETIO_HOST),
            port: Number(this.config.get<string>(REDIS_SOCKETIO_PORT)),
          },
        }),
      () =>
        this.tcp.pingCheck('redis-cache', {
          transport: Transport.TCP,
          options: {
            host: this.config.get<string>(REDIS_CACHE_HOST),
            port: Number(this.config.get<string>(REDIS_CACHE_PORT)),
          },
        }),
      () =>
        this.tcp.pingCheck('redis-queue', {
          transport: Transport.TCP,
          options: {
            host: this.config.get<string>(REDIS_QUEUE_HOST),
            port: Number(this.config.get<string>(REDIS_QUEUE_PORT)),
          },
        }),
    ]);
  }
}
