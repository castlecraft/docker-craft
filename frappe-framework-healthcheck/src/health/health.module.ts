import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthControllers } from './controllers';
import { HealthServices } from './services';

@Module({
  imports: [TerminusModule],
  controllers: [...HealthControllers],
  providers: [...HealthServices],
})
export class HealthModule {}
