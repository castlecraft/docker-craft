# Docker related files

- node-latest-gettext-base for generating templates in image during ci tests
- node-latest-headless-chrome for running tests on gitlab ci
- localizer for exposing host.docker.internal on linux for development
