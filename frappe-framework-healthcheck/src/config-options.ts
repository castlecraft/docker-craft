import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';
import * as Joi from '@hapi/joi';

export const GLOBAL_PREFIX = 'api';
export const SOCKETIO_HOST = 'SOCKETIO_HOST';
export const SOCKETIO_PORT = 'SOCKETIO_PORT';
export const MARIADB_HOST = 'MARIADB_HOST';
export const MARIADB_PORT = 'MARIADB_PORT';
export const REDIS_SOCKETIO_HOST = 'REDIS_SOCKETIO_HOST';
export const REDIS_SOCKETIO_PORT = 'REDIS_SOCKETIO_PORT';
export const REDIS_CACHE_HOST = 'REDIS_CACHE_HOST';
export const REDIS_CACHE_PORT = 'REDIS_CACHE_PORT';
export const REDIS_QUEUE_HOST = 'REDIS_QUEUE_HOST';
export const REDIS_QUEUE_PORT = 'REDIS_QUEUE_PORT';
export const configOptions: ConfigModuleOptions = {
  isGlobal: true,
  validationSchema: Joi.object({
    NODE_ENV: Joi.string()
      .valid('development', 'production', 'test', 'staging')
      .default('production'),
    [SOCKETIO_HOST]: Joi.string().required(),
    [SOCKETIO_PORT]: Joi.string().required(),
    [MARIADB_HOST]: Joi.string().required(),
    [MARIADB_PORT]: Joi.string().required(),
    [REDIS_SOCKETIO_HOST]: Joi.string().required(),
    [REDIS_SOCKETIO_PORT]: Joi.string().required(),
    [REDIS_CACHE_HOST]: Joi.string().required(),
    [REDIS_CACHE_PORT]: Joi.string().required(),
    [REDIS_QUEUE_HOST]: Joi.string().required(),
    [REDIS_QUEUE_PORT]: Joi.string().required(),
  }),
};
