import { ConfigService } from '@nestjs/config';
import {
  HealthCheckService,
  MicroserviceHealthIndicator,
} from '@nestjs/terminus';
import { Test, TestingModule } from '@nestjs/testing';
import { TcpCheckService } from './tcp-check.service';

describe('TcpCheckService', () => {
  let service: TcpCheckService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TcpCheckService,
        { provide: HealthCheckService, useValue: {} },
        { provide: MicroserviceHealthIndicator, useValue: {} },
        { provide: ConfigService, useValue: {} },
      ],
    }).compile();

    service = module.get<TcpCheckService>(TcpCheckService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
