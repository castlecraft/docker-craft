#!/bin/bash

function checkEnv() {
  if [[ -z "$NODE_ENV" ]]; then
    export NODE_ENV=production
  fi
  if [[ -z "$SOCKETIO_HOST" ]]; then
    echo "SOCKETIO_HOST is not set"
    exit 1
  fi
  if [[ -z "$SOCKETIO_PORT" ]]; then
    echo "SOCKETIO_PORT is not set"
    exit 1
  fi
  if [[ -z "$MARIADB_HOST" ]]; then
    echo "MARIADB_HOST is not set"
    exit 1
  fi
  if [[ -z "$MARIADB_PORT" ]]; then
    echo "MARIADB_PORT is not set"
    exit 1
  fi
  if [[ -z "$REDIS_CACHE_HOST" ]]; then
    echo "REDIS_CACHE_HOST is not set"
    exit 1
  fi
  if [[ -z "$REDIS_CACHE_PORT" ]]; then
    echo "REDIS_CACHE_PORT is not set"
    exit 1
  fi
  if [[ -z "$REDIS_QUEUE_HOST" ]]; then
    echo "REDIS_QUEUE_HOST is not set"
    exit 1
  fi
  if [[ -z "$REDIS_QUEUE_PORT" ]]; then
    echo "REDIS_QUEUE_PORT is not set"
    exit 1
  fi
  if [[ -z "$REDIS_SOCKETIO_HOST" ]]; then
    echo "REDIS_SOCKETIO_HOST is not set"
    exit 1
  fi
  if [[ -z "$REDIS_SOCKETIO_PORT" ]]; then
    echo "REDIS_SOCKETIO_PORT is not set"
    exit 1
  fi
}

function configureServer() {
  if [ ! -f .env ]; then
    envsubst '${NODE_ENV}
      ${SOCKETIO_HOST}
      ${SOCKETIO_PORT}
      ${MARIADB_HOST}
      ${MARIADB_PORT}
      ${REDIS_SOCKETIO_HOST}
      ${REDIS_SOCKETIO_PORT}
      ${REDIS_CACHE_HOST}
      ${REDIS_CACHE_PORT}
      ${REDIS_QUEUE_HOST}
      ${REDIS_QUEUE_PORT}' \
      < env.tmpl > .env
  fi
}

export -f configureServer

if [ "$1" = 'start' ]; then
  # Validate if environment variables are set.
  checkEnv
  # Configure server
  su craft -c "bash -c configureServer"
  # Start server
  su craft -c "node dist/main.js"
fi

exec runuser -u craft "$@"
