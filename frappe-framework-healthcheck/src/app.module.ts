import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { configOptions } from './config-options';
import { HealthModule } from './health/health.module';

@Module({
  imports: [ConfigModule.forRoot(configOptions), HealthModule],
})
export class AppModule {}
